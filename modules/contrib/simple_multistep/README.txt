## CONTENTS OF THIS FILE
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
## INTRODUCTION

Simple form multi step

 * For a full description of the module, visit the project page:
   [https://www.drupal.org/project/simple_multistep]

 * To submit bug reports and feature suggestions, or to track changes:
   [https://www.drupal.org/project/issues/simple_multistep]

## REQUIREMENTS

This module will requires field_group module dependencies.

## INSTALLATION
 
 * Install as you would normally install a contributed Drupal module. Visit:
   [https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules]
   for further information.

## CONFIGURATION

1. Install simple_multistep module (this module require fieldgroup module).
2. Go to Manage form display and click "Add group".
3. Choose Form step in drop down and configure this field group as you like.


## MAINTAINERS

Current maintainers:
 * [Vitaliy Bogomazyuk (VitaliyB98)](https://www.drupal.org/u/vitaliyb98)
 
## This project has been sponsored by:
 * [Internetdevels](https://www.drupal.org/internetdevels)
