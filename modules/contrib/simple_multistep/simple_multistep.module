<?php

/**
 * @file
 * Contains simple_multistep.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_multistep\MultistepController;

/**
 * Implements hook_form_alter().
 */
function simple_multistep_form_alter(&$form, FormStateInterface &$form_state) {
  // Check if form using form group multi step field.
  if (_check_form_multistep($form)) {
    /** @var \Drupal\simple_multistep\MultistepController $multiStep */
    if ($multiStep = $form_state->get('multistep_controller')) {
      $multiStep->updateStepInfo();
    }
    else {
      $multiStep = new MultistepController($form, $form_state);
    }

    $multiStep->rebuildForm($form);

    $form_state->set('multistep_controller', $multiStep);

    // Attach style library.
    $form['#attached']['library'][] = 'simple_multistep/simple_multistep';
  }
}

/**
 * Validator handler for next button.
 */
function simple_multistep_register_next_step(&$form, FormStateInterface $form_state) {
  /** @var \Drupal\simple_multistep\MultistepController $multiStep */
  $multiStep = $form_state->get('multistep_controller');

  // Need update form state after submission.
  $multiStep->setFormState($form_state);

  $multiStep->saveInputValues();
  $multiStep->saveStoredValues();

  $multiStep->increaseStep();

  // Fill field value previous step.
  $stored_input = $multiStep->getInputValues();

  $current_step = $multiStep->getCurrentStep();

  if (isset($stored_input[$current_step]) && !empty($stored_input[$current_step])) {
    $form_state->setUserInput($stored_input[$current_step]);
  }

  $form_state->set('multistep_controller', $multiStep);

  $form_state->setRebuild();
}

/**
 * Validator handler for back button.
 */
function simple_multistep_register_back(&$form, FormStateInterface $form_state) {
  /** @var \Drupal\simple_multistep\MultistepController $multiStep */
  $multiStep = $form_state->get('multistep_controller');

  // Need update form state after submission.
  $multiStep->setFormState($form_state);

  // If current_step more than 0.
  if ($multiStep->getCurrentStep()) {
    $multiStep->reduceStep();

    // Fill field value previous step.
    $stored_input = $multiStep->getInputValues();

    $current_step = $multiStep->getCurrentStep();

    if (isset($stored_input[$current_step]) && !empty($stored_input[$current_step])) {
      $form_state->setUserInput($stored_input[$current_step]);
    }

    $form_state->set('multistep_controller', $multiStep);
    $form_state->setRebuild();
  }

}

/**
 * Validation handler.
 */
function simple_multistep_multistep_validate(&$form, FormStateInterface &$form_state) {
  /** @var \Drupal\simple_multistep\MultistepController $multiStep */
  $multiStep = $form_state->get('multistep_controller');

  // Need update form state after submission.
  $multiStep->setFormState($form_state);

  $stored_values = $multiStep->getStoredValues();

  if (!empty($stored_values)) {
    foreach ($stored_values as $value_list) {
      foreach ($value_list as $field_name => $field_value) {
        $form_state->setValue($field_name, $field_value);
      }
    }
  }

  $form_state->set('multistep_controller', $multiStep);
}

/**
 * Check if valid multi step form.
 *
 * @param array $form
 *   Form array.
 *
 * @return bool
 *   TRUE if form multi step.
 */
function _check_form_multistep(array $form) {
  if (isset($form['#fieldgroups']) && !empty($form['#fieldgroups'])) {
    foreach ($form['#fieldgroups'] as $fieldgroup) {
      if (is_object($fieldgroup) && $fieldgroup->format_type == 'form_step') {
        return TRUE;
      }
    }
  }

  return FALSE;
}
