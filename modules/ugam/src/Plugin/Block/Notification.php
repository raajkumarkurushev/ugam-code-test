<?php

namespace Drupal\ugam\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;
use Drupal\Core\Link;

/**
 * Notofication Block
 *
 * @Block(
 *   id = "notofication_block",
 *   admin_label = @Translation("Notification Block"),
 * )
 */
class Notification extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $uid = \Drupal::currentUser()->id();
    if(!$uid) {
      return ;
    }
    $user = User::load(\Drupal::currentUser()->id());
    
    $dobTimestamp = $user->get('field_dob')->date->getTimestamp();
    $dobMonth = \Drupal::service('date.formatter')->format($dobTimestamp, 'custom', 'm');

    $currentTimestamp = \Drupal::time()->getCurrentTime();    
    $currentMonth = \Drupal::service('date.formatter')->format($currentTimestamp, 'custom', 'm');
    
    $favPizza = $user->get('field_pizza_type')->getValue()[0]['target_id'];
    
    $favPizzalink = Link::createFromRoute('here', 'entity.taxonomy_term.canonical', ['taxonomy_term' => $favPizza]);
    $favPizzalink = render($favPizzalink->toRenderable());
    
    
    if($dobMonth == $currentMonth) {
      $message = '<p>Thinking of you on your birthday and wishing you everything happy !!!</p>';
      $message .= "<p>We are offering you <blink>10% off on your favourite pizza</blink> for this special day !!!, To avail this offer click $favPizzalink</p>";
      \Drupal::messenger()->addStatus(['#markup' => $message]);
    }
    
    return ;
  }
  
  public function getCacheMaxAge() {
    return 0;
}
}